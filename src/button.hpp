#pragma once

#include <Arduino.h>

#include "component.hpp"
#include "handler.hpp"

namespace components {

class button : public component
{
  public:
    button( int pin )
      : _pin( pin )
      , _mode( INPUT_PULLUP )
      , _valuePress( LOW )
      , _state( IDLE )
      , _debounce_delay( 50 )
      , _long_press_delay( 5000 )
    {}

    virtual void setup()
    {
        pinMode( _pin, _mode );
    }

    virtual void update()
    {
        switch ( _state ) {
            case IDLE:
                if ( instant_button_status() ) {
                    switch_to( WAIT, millis() );
                }
                break;

            case WAIT:
                if ( !instant_button_status() ) {
                    switch_to( IDLE, 0 );
                } else if ( elapsed_time() >= _debounce_delay ) {
                    switch_to( PRESSED, millis() );
                }
                break;

            case PRESSED:
                if ( !instant_button_status() ) {
                    signal( _on_press );
                    switch_to( RELEASED, 0 );
                } else if ( elapsed_time() >= _long_press_delay ) {
                    signal( _on_long_press );
                    switch_to( LONG_PRESSED, 0 );
                }

                break;

            case LONG_PRESSED:
                if ( !instant_button_status() ) {
                    switch_to( RELEASED, 0 );
                }
                break;

            case RELEASED:
                switch_to( IDLE, 0 );
                break;

            default:
                break;
        }
    }

    void on_press( handler value )
    {
        _on_press = value;
    }

    void on_long_press( handler value )
    {
        _on_long_press = value;
    }

  private:
    enum State
    {
        IDLE,
        WAIT,
        PRESSED,
        LONG_PRESSED,
        RELEASED,
        ELSE
    };

    void switch_to( State state, uint32_t timer )
    {
        _state = state;
        _timer = timer;
    }

    bool instant_button_status()
    {
        return digitalRead( _pin ) == _valuePress;
    }

    uint16_t elapsed_time()
    {
        return millis() - _timer;
    }

    uint8_t _pin;

    uint8_t _mode;
    uint8_t _valuePress;
    uint8_t _debounce_delay = 50;
    uint16_t _long_press_delay = 5000;

    State _state = IDLE;
    uint32_t _timer;
    handler _on_press = nullptr;
    handler _on_long_press = nullptr;
};
}
