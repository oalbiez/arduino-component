#pragma once

#include <Arduino.h>

#include "component.hpp"

namespace components {

class buzzer : public component
{

  public:
    buzzer( int pin )
      : _pin( pin )
      , _status( false )
    {}

    virtual void setup()
    {}

    virtual void update()
    {}

    void beep( unsigned int frequency = 5000 )
    {
        tone( _pin, frequency );
        _status = true;
    }

    void off()
    {
        noTone( _pin );
        _status = false;
    }

    void toggle()
    {
        if ( _status ) {
            off();
        } else {
            beep();
        }
    }

  private:
    uint8_t _pin;
    bool _status;
};

}