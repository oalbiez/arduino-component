#pragma once

namespace components {

/**
 * The main component interface.
 * A component should be able to setup hardware and to be updated each loop.
 */
class component
{
  public:
    /**
     * Perform setup configuration.
     */
    virtual void setup() = 0;

    /**
     * Update the component
     */
    virtual void update() = 0;
};

}