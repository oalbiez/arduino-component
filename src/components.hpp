#pragma once

#include <list>

#include "component.hpp"

namespace components {

class components
{
  public:
    components()
      : _components()
    {}

    template<typename component_type>
    component_type* add( component_type* item )
    {
        _components.push_back( item );
        return item;
    }

    void setup()
    {
        for ( auto it = _components.begin(); it != _components.end(); ++it ) {
            ( *it )->setup();
        }
    }

    void update()
    {
        for ( auto it = _components.begin(); it != _components.end(); ++it ) {
            ( *it )->update();
        }
    }

  private:
    typedef std::list<component*> container;
    container _components;
};

}