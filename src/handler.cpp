#include "handler.hpp"

namespace components {

void
signal( handler& h )
{
    if ( h != nullptr ) {
        h();
    }
}

}