#pragma once

namespace components {

using handler = void ( * )();

void
signal( handler& h );

}