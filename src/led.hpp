#pragma once

#include <Arduino.h>

#include "component.hpp"

namespace components {

class led : public component
{
  public:
    led( int pin )
      : _pin( pin )
      , _status( false )
    {}

    virtual void setup()
    {
        pinMode( _pin, OUTPUT );
    }

    virtual void update()
    {}

    void on()
    {
        digitalWrite( _pin, HIGH );
        _status = true;
    }

    void off()
    {
        digitalWrite( _pin, LOW );
        _status = false;
    }

    void toggle()
    {
        if ( _status ) {
            off();
        } else {
            on();
        }
    }

  private:
    uint8_t _pin;
    bool _status;
};

}
