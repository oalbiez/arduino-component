#include <Arduino.h>

#include "button.hpp"
#include "buzzer.hpp"
#include "components.hpp"
#include "led.hpp"
#include "timer.hpp"
#include "unit.hpp"

#ifndef LED_BUILTIN
#define LED_BUILTIN 13
#endif

auto application = components::components();
auto builtin = application.add( new components::led( LED_BUILTIN ) );
auto sound = application.add( new components::buzzer( 3 ) );
auto toggle = application.add( new components::button( 2 ) );
auto blink = application.add( new components::timer( components::time::milliseconds( 500 ) ) );

void
setup()
{
    toggle->on_press( []() { sound->toggle(); } );

    toggle->on_long_press( []() { builtin->toggle(); } );
    blink->on_elapsed( []() { builtin->toggle(); } );

    application.setup();
}

void
loop()
{
    application.update();
}