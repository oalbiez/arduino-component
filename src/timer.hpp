#pragma once

#include <Arduino.h>

#include "component.hpp"
#include "handler.hpp"
#include "unit.hpp"

namespace components {

class timer : public component
{

  public:
    timer( time delay )
      : _delay( delay )
      , _start( time::milliseconds( millis() ) )
    {}

    virtual void setup()
    {}

    virtual void update()
    {
        if ( elapsed_time().to_milliseconds() >= _delay.to_milliseconds() ) {
            signal( _elapsed );
            start();
        }
    }

    void start()
    {
        _start = time::milliseconds( millis() );
    }

    void stop()
    {
        _start = time::milliseconds( 0 );
    }

    void on_elapsed( handler value )
    {
        _elapsed = value;
    }

  private:
    time elapsed_time()
    {
        return time::milliseconds( millis() - _start.to_milliseconds() );
    }

    time _delay;
    time _start;

    handler _elapsed = nullptr;
};

}