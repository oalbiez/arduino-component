#pragma once

#include <stdint.h>

namespace components {

class time
{
  public:
    static time milliseconds( uint32_t value )
    {
        return time( value );
    }

    static time seconds( uint32_t value )
    {
        return milliseconds( value * 1000 );
    }

    static time minutes( uint32_t value )
    {
        return seconds( value * 60 );
    }

    static time hours( uint32_t value )
    {
        return minutes( value * 60 );
    }

    static time days( uint32_t value )
    {
        return hours( value * 24 );
    }

    uint32_t to_milliseconds() const
    {
        return _value;
    }

    float to_seconds() const
    {
        return to_milliseconds() / 1000;
    }

    float to_minutes() const
    {
        return to_seconds() / 60;
    }

    float to_hours() const
    {
        return to_minutes() / 60;
    }

    float to_days() const
    {
        return to_days() / 24;
    }

  private:
    time( uint32_t value )
      : _value( value )
    {}

    uint32_t _value;
};

bool
operator==( time const& left, time const& right )
{
    return left.to_milliseconds() == right.to_milliseconds();
}

bool
operator!=( time const& left, time const& right )
{
    return !( left == right );
}

bool
operator<( time const& left, time const& right )
{
    return left.to_milliseconds() < right.to_milliseconds();
}

bool
operator<=( time const& left, time const& right )
{
    return left < right || left == right;
}

bool
operator>( time const& left, time const& right )
{
    return !( left < right ) && left != right;
}

bool
operator>=( time const& left, time const& right )
{
    return !( left < right );
}

time
operator+( time const& left, time const& right )
{
    return time::milliseconds( left.to_milliseconds() + right.to_milliseconds() );
}

}
